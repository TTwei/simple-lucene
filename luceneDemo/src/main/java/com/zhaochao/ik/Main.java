package com.zhaochao.ik;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.wltea.analyzer.cfg.Configuration;
import org.wltea.analyzer.cfg.DefaultConfig;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import org.wltea.analyzer.dic.Dictionary;
import org.wltea.analyzer.dic.Hit;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class Main {
	public static void main(String[] args) throws IOException {
		String s = "JAVA是一个好语言，从到2015年12月1日它已经有20年的历史了";
	     queryWords(s);
	}
	
	public static void queryWords(String query) throws IOException {
		Configuration cfg = DefaultConfig.getInstance();
		System.out.println(cfg.getMainDictionary()); // 系统默认词库
		System.out.println(cfg.getQuantifierDicionary());
		List<String> list = new ArrayList<String>();
		StringReader input = new StringReader(query.trim());
		IKSegmenter ikSeg = new IKSegmenter(input, true);   // true 用智能分词 ，false细粒度
		for (Lexeme lexeme = ikSeg.next(); lexeme != null; lexeme = ikSeg.next()) {
			System.out.print(lexeme.getLexemeText()+"|");
		}
	
	}
	
	private static void testAddWord() throws IOException {
		String s = "中文分词工具包赵超张章";
		Configuration cfg = DefaultConfig.getInstance();
		cfg.setUseSmart(true); // 设置智能分词
		Dictionary.initial(cfg);
		Dictionary dictionary = Dictionary.getSingleton();
		List<String> words = new ArrayList<String>();
		words.add("张翔");
		words.add("丁迁迁");
		dictionary.addWords(words);  // 自动添加自定义词
		System.out.println(cfg.getMainDictionary()); // 系统默认词库
		System.out.println(cfg.getQuantifierDicionary());
		Hit hit = dictionary.matchInMainDict("赵超".toCharArray());
		System.out.println(hit.isMatch());
		//System.out.println(queryWords(s));
	}





}
