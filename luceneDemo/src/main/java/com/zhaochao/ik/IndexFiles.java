package com.zhaochao.ik;

import org.apache.lucene.analysis.Analyzer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.zhaochao.dao.DBUtill;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class IndexFiles {

	public static void main(String[] args) throws IOException, ParseException {
		// indexFile();
		search();
		//deleteIndex("1");
	}

	public static void search() throws IOException, ParseException {
		Analyzer analyzer = new IKAnalyzer();
		IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get("C:/MyIndex")));
		IndexSearcher searcher = new IndexSearcher(reader);
		QueryParser parser = new QueryParser("name", analyzer);
		String queries = "韩国";
		Query query = parser.parse(queries);
		System.out.println("Searching for: " + query.toString(queries));
		TopDocs results = searcher.search(query, 10);
		System.out.println("Total match：" + results.totalHits);
		ScoreDoc[] hits = results.scoreDocs;
		int count = 1;
		for (ScoreDoc hit : hits) {
			Document doc1 = searcher.doc(hit.doc);
			String res = doc1.get("name");
			System.err.println(count + "  " + res + ", " + hit.score);
			count++;
		}

	}

	public static void indexFile() throws IOException {
		Directory dir = FSDirectory.open(Paths.get("C:/MyIndex"));
		Analyzer analyzer = new IKAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		IndexWriter writer = new IndexWriter(dir, iwc);
		String sql="select * from zc_search_res";
		List<Map<String,String>> data=DBUtill.getData(sql);
		for(Map<String,String> map:data){
			Document doc = new Document();
			for(Entry<String, String> en:map.entrySet()){
				doc.add(new StringField(en.getKey(), en.getValue()==null?"":en.getValue(), Store.YES)); 
			}
			writer.addDocument(doc);
		}

		writer.close();
	}
	
	public static void deleteIndex(String id) throws IOException{
		Directory dir = FSDirectory.open(Paths.get("C:/MyIndex"));
		Analyzer analyzer = new IKAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		IndexWriter writer = new IndexWriter(dir, iwc);
		Term term = new Term("id", id);
		writer.deleteDocuments(term);
		writer.close();
	}
	

}
