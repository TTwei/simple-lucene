package com.zhaochao.okhttp;

import java.io.IOException;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class Main {

	public static void main(String[] args) {
		while (true) {
			 get();
		}
	
		// post();
	}

	private static void post() {
		OkHttpClient mOkHttpClient = new OkHttpClient();
		FormEncodingBuilder builder = new FormEncodingBuilder();
		builder.add("username", "张鸿洋");

		Request request = new Request.Builder().url("http://www.baai.com").post(builder.build()).build();
		mOkHttpClient.newCall(request).enqueue(new Callback() {

			public void onFailure(Request arg0, IOException arg1) {
				// TODO Auto-generated method stub

			}

			public void onResponse(Response arg0) throws IOException {
				// TODO Auto-generated method stub
				System.out.println(arg0);
			}

		});
	}

	private static void get() {
		// 创建okHttpClient对象
		OkHttpClient mOkHttpClient = new OkHttpClient();
		// 创建一个Request
		final Request request = new Request.Builder().url("http://www.lizi.com/itemSearch/search?content=CANMAKE").build();
		// new call
		Call call = mOkHttpClient.newCall(request);
		// 请求加入调度
		call.enqueue(new Callback() {
			public void onFailure(Request arg0, IOException arg1) {
				System.err.println("error" + arg1.getMessage());
			}

			public void onResponse(Response arg0) throws IOException {
				System.out.println(arg0);
			}
		});
		System.out.println("==================");
	}

}
