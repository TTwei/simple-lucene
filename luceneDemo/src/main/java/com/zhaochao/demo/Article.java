package com.zhaochao.demo;

public class Article {
	private String titleHightLight;
	private String contentHightLight;
	public String getTitleHightLight() {
		return titleHightLight;
	}
	public void setTitleHightLight(String titleHightLight) {
		this.titleHightLight = titleHightLight;
	}
	public String getContentHightLight() {
		return contentHightLight;
	}
	public void setContentHightLight(String contentHightLight) {
		this.contentHightLight = contentHightLight;
	}
	private String id;
	private String title;
	private String content;
	public String getPubTime() {
		return pubTime;
	}
	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	private String pubTime;
	private String channelId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", content=" + content + ", pubTime=" + pubTime
				+ ", channelId=" + channelId + "]";
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
