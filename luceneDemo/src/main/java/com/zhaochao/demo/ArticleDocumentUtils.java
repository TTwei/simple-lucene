package com.zhaochao.demo;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;


public class ArticleDocumentUtils {

    /**
     * 把Article转为Document
     * 
     * @param article
     * @return
     */
    public static Document articleToDocument(Article article) {
        Document doc = new Document();
        doc.add(new StringField("id", article.getId(), Field.Store.YES));
        doc.add(new TextField("title", article.getTitle(), Field.Store.YES));
        doc.add(new TextField("content", article.getContent(), Field.Store.YES));
        doc.add(new StringField("channelId",article.getChannelId(), Field.Store.YES));
        doc.add(new StringField("pubTime",article.getPubTime(), Field.Store.YES)); 
        return doc;
    }

    /**
     * 把Document转为Article
     * 
     * @param doc
     * @return
     */
    public static Article documentToArticle(Document doc) {
        Article article = new Article();
        article.setId(doc.get("id"));
        article.setTitle(doc.get("title"));
        article.setContent(doc.get("content"));
        article.setPubTime(doc.get("pubTime"));
        article.setChannelId(doc.get("channelId"));
        return article;
    }

}