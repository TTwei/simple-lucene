package com.zhaochao.demo;


import java.io.IOException;

import java.nio.file.Paths;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class LuceneUtils {

    private static Directory directory; // 索引库目录
    private static Analyzer analyzer; // 分词器
    private static IndexWriter indexWriter;
    private static IndexReader indexReader;

    static {
        try {
            // 这里应是读取配置文件得到的索引库目录
            directory = FSDirectory.open(Paths.get("indexDir"));
            analyzer = new IKAnalyzer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取全局唯一的IndexWriter对象
     * 
     * @return
     */
    public static IndexWriter getIndexWriter() {
        // 在第一次使用IndexWriter是进行初始化
        if (indexWriter == null) {
            synchronized (LuceneUtils.class) { // 注意线程安全问题
                if (indexWriter == null) {
                    try {
                    	IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                	    indexWriter = new IndexWriter(directory, iwc);
                        System.out.println("=== 已经初始化 IndexWriter ===");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            // 指定一段代码，会在JVM退出之前执行。
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    try {
                        indexWriter.close();
                        System.out.println("=== 已经关闭 IndexWriter ===");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        return indexWriter;
    }

    
    public static IndexReader getIndexReader() {
        // 在第一次使用IndexWriter是进行初始化
        if (indexReader == null) {
            synchronized (LuceneUtils.class) { // 注意线程安全问题
                if (indexReader == null) {
                    try {
                       indexReader = DirectoryReader.open(directory);
                        System.out.println("=== 已经初始化 reader ===");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            // 指定一段代码，会在JVM退出之前执行。
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    try {
                    	indexReader.close();
                        System.out.println("=== 已经关闭 indexReader ===");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        return indexReader;
    }
    
    
    public static Directory getDirectory() {
        return directory;
    }

    public static Analyzer getAnalyzer() {
        return analyzer;
    }

}