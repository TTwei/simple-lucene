package com.zhaochao.demo;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;

import com.zhaochao.dao.DBUtill;

public class ArticleIndexDaoTest {

	private ArticleIndexDao indexDao = new ArticleIndexDao();

	public static void main(String[] args) {

		ArticleIndexDaoTest test = new ArticleIndexDaoTest();

		// test.testSave();
		// test.testSave_25();
		// test.testUpdate();
		// test.testDelete();
		test.testSearch();

	}

	public void testSearch() {
		// 准备查询条件
		String queryString = "浙江";
		Term term2 = new Term("channelId", "16030103");
		TermQuery termQuery = new TermQuery(term2);
		QueryResult qr = indexDao.search(queryString, 0, 10, null);
		// 显示结果
		System.err.println("总结果数：" + qr.getCount());
		for (Article a : (List<Article>) qr.getList()) {
			System.out.println("------------------------------");
			System.out.println("id = " + a.getId());
			System.out.println("title = " + a.getTitle());
			System.out.println(a.getTitleHightLight());
			System.out.println(a.getContentHightLight());
			System.out.println("pubdata=" + a.getPubTime());
			System.out.println("channelId=" + a.getChannelId());
		}
	}

	public void testSave() {
		// 准备数据
		List<Map<String, String>> data = DBUtill.getData("");
		for (Map<String, String> map : data) {
			Document doc = new Document();
			Article article = new Article();
			article.setId(map.get("id"));
			article.setTitle(map.get("name"));
			article.setContent(map.get("add_description"));
			System.out.println("add " + article.toString());
			// 放到索引库中
			indexDao.save(article);
		}

	}

	public void testSave_25() {
		for (int i = 1; i <= 25; i++) {
			// 准备数据
			Article article = new Article();
			article.setId(i + "");
			article.setTitle("准备Lucene的开发环境");
			article.setContent("如果信息检索系统在用户发出了检索请求后再去互联网上找答案，根本无法在有限的时间内返回结果。");
			// 放到索引库中
			indexDao.save(article);
		}
	}

	public void testDelete() {
		indexDao.delete("1");
	}

	public void testUpdate() {
		// 准备数据
		Article article = new Article();
		article.setId("1");
		article.setTitle("准备Lucene的开发环境");
		article.setContent("这是更新后的内容");
		// 更新到索引库中
		indexDao.update(article);
	}
	// 用于分页的

}