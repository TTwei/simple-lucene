package com.zhaochao.dao;

public class TableHead {
//	数据库中表名
	private String    tableName;
//	表中列数量
	private int       tableColumn;
//	表中列名
	private String [] tableColumnName;
	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getTableColumn() {
		return tableColumn;
	}

	public void setTableColumn(int tableColumn) {
		this.tableColumn = tableColumn;
	}

	public String getTableColumnName(int i) {
		return tableColumnName[i];
	}

	public void setTableColumnName(String tableColumnName,int i) {
		this.tableColumnName[i-1] = tableColumnName;
	}

	public TableHead(int count){
		this.tableColumn=count;
		this.tableColumnName=new String [count];
	}
}
