package com.zhaochao.dao;

public class TableContent {
	// 表字段内容
	private String[] tableContent;

	public TableContent(int Count) {
		this.tableContent = new String[Count];
	}

	public String getTableContent(int i) {
		return tableContent[i];
	}

	public void setTableContent(String tableContent, int i) {
		this.tableContent[i - 1] = tableContent;
	}

}
