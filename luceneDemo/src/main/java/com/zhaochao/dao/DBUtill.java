package com.zhaochao.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;

import com.zhaochao.demo.Article;
import com.zhaochao.demo.ArticleIndexDao;
import com.zhaochao.utill.StringUtill;

import org.apache.lucene.document.Field.Store;

public class DBUtill {

	public static String getSQL() {

		String sql = "SELECT id as id,PUB_DATE as pubData,CHANNEL_ID as channelId,ATTRIBUTE_VALUE as content,TITLE as title from search  ";

		return sql;
	}

	public static void main(String[] rags) throws SQLException {

		List<Map<String, String>> data = DBUtill.getData(getSQL());
		 ArticleIndexDao indexDao = new ArticleIndexDao();
		 try {
				for (Map<String, String> map : data) {
					Article news=new Article();
					news.setId(map.get("id"));
					news.setPubTime(map.get("PUB_DATE"));
					news.setChannelId(map.get("CHANNEL_ID"));
					news.setContent(StringUtill.delHTMLTag(map.get("ATTRIBUTE_VALUE")));
					news.setTitle(map.get("TITLE"));
					System.out.println("add "+news);
					indexDao.save(news);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 


	}

	public static List<Map<String, String>> getData(String sql) {
		String[] paramters = null;
		iConn iconn = new MySQLConn();
		MySQL mysql = null;
		Table table = null;
		try {
			mysql = new MySQL(iconn);
			table = mysql.ExecuteSQL(sql, paramters);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		for (int k = 0; k < table.contentSize(); k++) {
			Map<String, String> map = new HashMap<String, String>();
			for (int j = 0; j < table.getColoumnCount(); j++) {
				map.put(table.getTablehead().getTableColumnName(j), table.getContent().get(k).getTableContent(j));
			}
			data.add(map);
		}
		return data;
	}

}
