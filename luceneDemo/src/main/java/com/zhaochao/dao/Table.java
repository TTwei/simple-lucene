package com.zhaochao.dao;

import java.util.ArrayList;
import java.util.List;

public class Table {
	// 表中列数量
	private int coloumnCount;
	// 表头信息
	private TableHead tablehead;
	// 表个字段
	private List<TableContent> content;

	// 向表中增加字段
	public void addContent(TableContent content) {
		this.content.add(content);
	}

	// 表中字段大小
	public int contentSize() {
		return this.content.size();
	}

	// 表列数
	public int getColoumnCount() {
		return coloumnCount;
	}

	public void setColoumnCount(int coloumnCount) {
		this.coloumnCount = coloumnCount;
	}

	public TableHead getTablehead() {
		return tablehead;
	}

	public void setTablehead(TableHead tablehead) {
		this.tablehead = tablehead;
	}

	public List<TableContent> getContent() {
		return content;
	}

	public void setContent(List<TableContent> content) {
		this.content = content;
	}

	public Table(int Count) {
		this.coloumnCount = Count;
		this.content = new ArrayList<TableContent>();
	}

	// 得到表中第i个字段
	public TableContent getTableContent(int i) {
		return this.content.get(i);
	}
}
