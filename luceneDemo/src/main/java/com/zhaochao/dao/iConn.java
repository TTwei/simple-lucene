package com.zhaochao.dao;

import java.sql.Connection;
public interface iConn {
	Connection getConn() throws Exception;
}
