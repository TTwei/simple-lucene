package com.zhaochao.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MYSQLUtill {
	private static final String DBDRIVER = "org.gjt.mm.mysql.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/test";
	private static final String DBUSER = "root";
	private static final String DBPASS = "wh5268925";

	public static void main(String[] args) throws SQLException {
		List<String> colums = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		colums.add("IS_TOPIC");
		colums.add("STATE");
		values.add("1");
		values.add("2");

		addNews(colums, values);
	}

	public static void addNews(List<String> colums, List<String> values) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			Class.forName(DBDRIVER).newInstance();
			con = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
			StringBuffer sql = new StringBuffer("insert into search  ( ");
			sql.append(colums.get(0));
			for (int i = 1; i < colums.size(); i++) {
				sql.append(",");
				sql.append(colums.get(i));
			}
			sql.append(")");
			sql.append(" value ( ");

			sql.append("?");
			for (int i = 1; i < values.size(); i++) {
				sql.append(",");
				sql.append("?");
			}
			sql.append(")");
			System.out.println(sql.toString());

			ps = con.prepareStatement(sql.toString());
			
			for(int i=0;i<values.size();i++){
				ps.setString(i+1, values.get(i));
			}
			
		
			
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			ps.close();
			con.close();
		}
	}
}
